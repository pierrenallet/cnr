﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cnr.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";
			throw new InvalidOperationException("WOOOO");
			return View();
		}

		const string connectionString = @"
Server=tcp:cnrdb.database.windows.net,1433;Initial Catalog=cnrdatabase;Persist Security Info=False;User ID=pierre;Password=P@ssw0rd12345;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
		public ActionResult Contact()
		{
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = @"select top 10 * from SalesLT.Customer";
				command.CommandType = System.Data.CommandType.Text;
				 var reader = command.ExecuteReader();
				ViewBag.Message = "Your contact page.";
				while (reader.Read())
				{
					string s = ViewBag.Message;
					s += reader[0];
					ViewBag.Message = s;
					Trace.WriteLine("id = " + reader[0]);
				}

				return View();
			}
		}
	}
}